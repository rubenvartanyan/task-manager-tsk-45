package ru.vartanyan.tm.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Project;
import ru.vartanyan.tm.enumerated.Status;

import java.util.List;

public interface IProjectService extends IBusinessService<Project> {

    Project add(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @SneakyThrows
    void remove(@Nullable Project entity);

    @SneakyThrows
    void clear(@Nullable String userId);

    @NotNull
    @SneakyThrows
    List<Project> findAll(@Nullable String userId);

    @SneakyThrows
    @NotNull Project findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    @SneakyThrows
    Project findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @NotNull
    @SneakyThrows
    Project findOneByName(
            @Nullable String userId,
            @Nullable String name
    );

    @SneakyThrows
    void remove(
            @Nullable String userId,
            @Nullable Project entity
    );

    @SneakyThrows
    void removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @SneakyThrows
    void removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    @SneakyThrows
    void removeOneByName(
            @Nullable String userId,
            @Nullable String name
    );

    @SneakyThrows
    void changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @SneakyThrows
    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );
}


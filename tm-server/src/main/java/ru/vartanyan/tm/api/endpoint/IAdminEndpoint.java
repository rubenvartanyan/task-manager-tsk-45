package ru.vartanyan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminEndpoint {

    @WebMethod
    void clearProject(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws AccessDeniedException;

    @WebMethod
    void clearTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) throws AccessDeniedException;

    @WebMethod
    List<Session> listSession(
            @WebParam(name = "session", partName = "session"
            ) @Nullable final Session session
    ) throws AccessDeniedException;

}
